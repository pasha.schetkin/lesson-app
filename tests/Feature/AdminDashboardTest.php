<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminDashboardTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     * @group admin
     * @return void
     */
    public function test_admin_dashboard()
    {
        $response = $this->get(route('admin.dashboard'));
        $response->assertRedirect();
    }

    /**
     * A basic feature test example.
     * @group admin
     * @return void
     */
    public function test_simple_user_cannot_get_admin_dashboard()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $response = $this->get(route('admin.dashboard'));
        $response->assertRedirect();
    }

    /**
     * @group admin
     * @return void
     */
    public function test_simple_user_cannot_see_admin_comments_page()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $response = $this->get(route('admin.comments.index'));
        $response->assertRedirect();
    }

    /**
     * @group admin
     * @return void
     */
    public function test_simple_user_cannot_see_admin_users_page()
    {
        $user = \App\Models\User::factory()->create();
        $this->actingAs($user);
        $response = $this->get(route('admin.users.index'));
        $response->assertRedirect();
    }

    /**
     * @group admin
     * @return void
     */
    public function test_simple_user_cannot_see_admin_comment_detail_view()
    {
        $user = \App\Models\User::factory()->create();
        $another_user = \App\Models\User::factory()->create();
        $comment = \App\Models\Comment::factory()
            ->for($another_user)
            ->for(\App\Models\Article::factory())
            ->create();
        $this->actingAs($user);
        $response = $this->get(route('admin.comments.show', compact('comment')));
        $response->assertRedirect();
    }

    /**
     * @group admin
     * @return void
     */
    public function test_admin_successfully_see_admin_dashboard()
    {
        $admin = \App\Models\User::factory()->has(\App\Models\Role::factory()->state(['name' => 'Admin']))->create();
        $this->actingAs($admin);
        $response = $this->get(route('admin.dashboard'));
        $response->assertStatus(200);
    }
}
