<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $approve = $request->get('approve') ?? true;
        $comments = Comment::where('moderation', $approve)->get();
        return view('admin.comments.index', compact('comments'));
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return Application|Factory|View
     */
    public function show(Request $request, Comment $comment)
    {
        return view('admin.comments.show', compact('comment'));
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return JsonResponse
     */
    public function approve(Request $request, Comment $comment): JsonResponse
    {
        $comment->moderation = true;
        $comment->save();
        return response()->json([], 200);
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Request $request, Comment $comment): RedirectResponse
    {
        $comment->delete();
        return redirect()->route('admin.comments.index', ['approve' => false]);
    }
}
