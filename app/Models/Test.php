<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    public function getPictureUrl()
    {
        if (env('APP_ENV') == 'local') {
            $array = explode('/', $this->picture);
            $path = end($array);
            return 'pictures/' . $path;
        }
        return $this->picture;
    }
}
