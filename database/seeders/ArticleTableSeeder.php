<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::all();
        foreach ($users as $user) {
            \App\Models\Article::factory()->count(rand(7, 9))->for($user)
                ->create();
        }
    }
}
