<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::all();
        $articles = \App\Models\Article::all();
        foreach ($articles as $article) {
            foreach ($users as $user) {
                \App\Models\Comment::factory()
                    ->count(rand(1, 3))
                    ->for($article)
                    ->for($user)
                    ->create();
            }
        }
    }
}
