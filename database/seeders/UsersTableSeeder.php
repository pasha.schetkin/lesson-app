<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->state(
            ['email' => 'admin@admin.com']
        )->has(
            \App\Models\Role::factory()->state(
                ['name' => 'Admin']
            )
        )->create();

        $roles = \App\Models\Role::where('name', '!=', 'Admin')->get();
        \App\Models\User::factory()
            ->count(3)
            ->create();

        \App\Models\User::all()->each(function ($user) use ($roles) {
            $user->roles()->attach(
                $roles->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }
}
