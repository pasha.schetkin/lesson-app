<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['Moderator', 'BlackList', 'Editor'];
        foreach ($roles as $role)
        {
            \App\Models\Role::factory()->state(['name' => $role])->create();
        }
    }
}
