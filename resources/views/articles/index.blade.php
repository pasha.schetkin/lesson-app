@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col">
            <h3>Articles
                @can('create', \App\Models\Article::class)
                <a href="{{route('articles.create')}}" class="btn btn-primary btn-sm active" role="button"
                   aria-pressed="true">Create new Article</a>
                @endcan
            </h3>
        </div>
    </div>
    <div class="row">
        @foreach($articles as $article)
            <div class="col" style="padding: 35px 0 0 0;">
                <div class="card" style="width: 18rem;">
                    <div class="card-header">
                        {{ $article->title }}
                        <span class="badge badge-info" style="cursor: pointer" title="Comments count">
                            {{$article->moderationComments()->count()}}
                        </span>
                        @can('edit', $article)
                            <span class="edit-article" title="Edit {{$article->title}}">
                                <a href="{{route('articles.edit', ['article' => $article])}}" class="bi bi-pencil"></a>
                            </span>
                        @endcan
                        @can('delete', $article)
                        <form action="{{route('articles.destroy', compact('article'))}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-sm btn-outline-danger">Delete article</button>
                        </form>
                        @endcan
                    </div>
                    <div class="card-body">
                        <blockquote class="blockquote mb-0">
                            <p class="text-truncate">
                                {{ $article->content }}
                            </p>
                        </blockquote>
                        <a href="{{route('articles.show', ['article' => $article])}}" class="card-link">Show more
                            ...</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row justify-content-md-center p-3">
        <div class="col col-lg-3">
            {{$articles->links('pagination::bootstrap-4')}}
        </div>
    </div>
@endsection
