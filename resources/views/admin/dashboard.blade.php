@extends('layouts.app')

@section('content')
    <h2>Hello admin!</h2>
    <a role="button" href="{{route('admin.comments.index')}}" class="btn btn-primary">
        All comments
    </a>
    <a role="button" href="{{route('admin.comments.index', ['approve' => false])}}" class="btn btn-primary">
        Need approve
    </a>
    <a role="button" href="{{route('admin.users.index')}}" class="btn btn-primary">
        Users
    </a>
    <h1>Hello, {{Auth::user()->name}}</h1>
@endsection
