@extends('layouts.app')

@section('content')
    <h2>Comments</h2>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Author</th>
            <th>Comment</th>
            <th colspan="2">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr id="comment-body-{{$comment->id}}">
                <td>{{$comment->id}}</td>
                <td>{{$comment->user->name}}</td>
                <td>{{$comment->body}}</td>
                <td colspan="2">
                    <form style="display: inline-block;"
                          action="{{route('admin.comments.delete', compact('comment'))}}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                    @if(!$comment->moderation)
                        <button data-csrf-token="{{csrf_token()}}" id="approve-{{$comment->id}}" data-comment-id="{{$comment->id}}" class="approve-comment btn btn-success btn-sm">Approve</button>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>
@endsection
