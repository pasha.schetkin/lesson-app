@extends('layouts.app')

@section('content')
    <h2>User: {{$user->name}}</h2>

    <p>
    <form action="{{route('admin.users.update', compact('user'))}}" method="post">
        @csrf
        @method('patch')
        <div class="row">
            <div class="col">
                <select name="roles[]" style="width: 100%; height: 150px" class="form-select" multiple aria-label="multiple select example">
                    @foreach($roles as $role)
                        @if($user->hasRole($role->name))
                            <option value="{{$role->id}}" selected>{{$role->name}}</option>
                        @else
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>

        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
    </p>
@endsection
